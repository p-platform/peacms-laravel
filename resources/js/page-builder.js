import grapesjs from 'grapesjs';
import $ from 'jquery';

import 'grapesjs-blocks-bootstrap4/dist/grapesjs-blocks-bootstrap4.min';

const editor = grapesjs.init({
    commands: {
        defaults: [
            {
                id: 'aaaa',
                run() {
                    alert('This is my command');
                },
            }
        ],
    },
    container: '#gjs',
    fromElement: true,
    height: '100vh',
    width: 'auto',
    storageManager: {
        type: 'remote',
        stepsBeforeSave: 0,
        urlStore: `${window.baseUrl}api/page/store/${$('#gjs').data('page')}`,
        urlLoad: `${window.baseUrl}api/page/store/${$('#gjs').data('page')}`,
        params: { _some_token: '....' },
        headers: {
            Authorization: 'Basic ...',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    },
    plugins: ['grapesjs-blocks-bootstrap4'],
    pluginsOpts: {
        'grapesjs-blocks-bootstrap4': {

        }
    },
    assetManager: {
        upload: `${window.baseUrl}api/page/upload/${$('#gjs').data('page')}`,
        uploadName: 'uploadFile',
        multiUpload: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
    },
    canvas: {
        styles: [
            `${window.baseUrl}css/bootstrap.css`,
        ],
        scripts: [
            `${window.baseUrl}js/jquery.min.js`,
            `${window.baseUrl}js/popper.min.js`,
            `${window.baseUrl}js/bootstrap.min.js`,
        ],
    }
});

window.editor = editor;
