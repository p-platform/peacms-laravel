const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath('./public')

mix.js('resources/js/page-builder.js', 'public/js')
    .js('node_modules/grapesjs-preset-webpage/dist/grapesjs-preset-webpage.min.js', 'public/js')
    .js('node_modules/bootstrap/dist/js/bootstrap.min.js', 'public/js')
    .js('node_modules/popper.js/dist/popper.min.js', 'public/js')
    .js('node_modules/jquery/dist/jquery.min.js', 'public/js')
    .sass('node_modules/bootstrap/scss/bootstrap.scss', 'public/css')
    .sass('resources/css/page-builder.scss', 'public/css')
