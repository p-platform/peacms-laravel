<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('peacms.')->middleware('web')->group(function () {
    Route::get('/tag/{peacms_post_category}', [\QuangPhuc\PeaCMS\Controllers\Front\CategoryController::class, 'index'])->name('post-category');
    Route::get('/post/{peacms_post}', [\QuangPhuc\PeaCMS\Controllers\Front\PostController::class, 'index'])->name('post');
    Route::get('/page/{peacms_page}', [\QuangPhuc\PeaCMS\Controllers\Front\PageController::class, 'index'])->name('page');
});

Route::name('peacms.api.')->prefix('api/')->middleware('web')->group(function () {
    Route::get('/page/store/{peacms_page}', [\QuangPhuc\PeaCMS\Controllers\Back\PageController::class, 'loadPageContent'])->name('page');
    Route::post('/page/store/{peacms_page}', [\QuangPhuc\PeaCMS\Controllers\Back\PageController::class, 'storePageContent'])->name('page');
    Route::post('/page/upload/{peacms_page}', [\QuangPhuc\PeaCMS\Controllers\Back\PageController::class, 'uploadAssets'])->name('page');
});


Route::name('peacms.back.')->prefix('back/')->middleware('web')->group(function () {
    Route::get('/page-builder/{peacms_page}', [\QuangPhuc\PeaCMS\Controllers\Back\PageController::class, 'pageBuilder'])->name('page');
});
