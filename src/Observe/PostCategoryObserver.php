<?php

namespace QuangPhuc\PeaCMS\Observe;

use QuangPhuc\PeaCMS\Model\PostCategory;

class PostCategoryObserver
{
    /**
     * Handle the post category "created" event.
     *
     * @param  PostCategory  $postCategory
     * @return void
     */
    public function created(PostCategory $postCategory)
    {
        //
    }

    /**
     * Handle the post category "updated" event.
     *
     * @param  PostCategory  $postCategory
     * @return void
     */
    public function updated(PostCategory $postCategory)
    {
        //
    }

    /**
     * Handle the post category "deleted" event.
     *
     * @param  PostCategory  $postCategory
     * @return void
     */
    public function deleted(PostCategory $postCategory)
    {
        //
    }

    /**
     * Handle the post category "restored" event.
     *
     * @param  PostCategory  $postCategory
     * @return void
     */
    public function restored(PostCategory $postCategory)
    {
        //
    }

    /**
     * Handle the post category "force deleted" event.
     *
     * @param  PostCategory  $postCategory
     * @return void
     */
    public function forceDeleted(PostCategory $postCategory)
    {
        //
    }
}
