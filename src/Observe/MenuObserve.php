<?php

namespace QuangPhuc\PeaCMS\Observe;

use QuangPhuc\PeaCMS\Model\Menu;

class MenuObserve
{
    /**
     * Handle the menu "created" event.
     *
     * @param  Menu  $menu
     * @return void
     */
    public function created(Menu $menu)
    {
        //
    }

    /**
     * Handle the menu "updated" event.
     *
     * @param  Menu  $menu
     * @return void
     */
    public function updated(Menu $menu)
    {
        //
    }

    /**
     * Handle the menu "deleted" event.
     *
     * @param  Menu  $menu
     * @return void
     */
    public function deleted(Menu $menu)
    {
        //
    }

    /**
     * Handle the menu "restored" event.
     *
     * @param  Menu  $menu
     * @return void
     */
    public function restored(Menu $menu)
    {
        //
    }

    /**
     * Handle the menu "force deleted" event.
     *
     * @param  Menu  $menu
     * @return void
     */
    public function forceDeleted(Menu $menu)
    {
        //
    }
}
