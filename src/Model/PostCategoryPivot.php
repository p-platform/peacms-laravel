<?php

namespace QuangPhuc\PeaCMS\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class PostCategoryPivot
 * @package QuangPhuc\PeaCMS\Model
 * @property Carbon $created_at
 */
class PostCategoryPivot extends Model
{
    use HasFactory;
    protected $table = 'post_category_pivot';
}
