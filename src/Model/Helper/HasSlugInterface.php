<?php
/**
 * HasSlugInterface.php
 * ${CARET}
 *
 * [] - [quang_phuc] - [04/10/2020]
 */

namespace QuangPhuc\PeaCMS\Model\Helper;

/**
 * Class HasSlugInterface
 * @package QuangPhuc\PeaCMS\Model\Helper
 * @property string $slug
 */
class HasSlugInterface {

}
