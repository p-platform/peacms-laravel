<?php
/**
 * HasSlug.php
 * ${CARET}
 *
 * [] - [quang_phuc] - [04/10/2020]
 */

namespace QuangPhuc\PeaCMS\Model\Helper;


use Illuminate\Database\Eloquent\Builder;

trait HasSlug {
    /**
     * Begin querying the model.
     *
     * @return Builder
     */
    public static abstract function query();

    public static function findBySlug($slug) {
        return static::query()->where('slug', $slug)->firstOrFail();
    }

    public function getRouteKeyName() {
        return 'slug';
    }
}
