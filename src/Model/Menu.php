<?php

namespace QuangPhuc\PeaCMS\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Menu
 * @package QuangPhuc\PeaCMS\Model
 * @property string $name
 * @property mixed $content
 * @property Carbon $created_at
 */
class Menu extends Model
{
    use HasFactory;
    protected $table = 'menu';
}
