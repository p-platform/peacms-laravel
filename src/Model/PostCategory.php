<?php

namespace QuangPhuc\PeaCMS\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use QuangPhuc\PeaCMS\Factory\PostCategoryFactory;
use QuangPhuc\PeaCMS\Model\Helper\HasSlug;

/**
 * Class PostCategory
 * @package QuangPhuc\PeaCMS\Model
 * @property string $name
 * @property Post[] $posts
 * @property PostCategory[] $parent
 * @property Carbon $created_at
 */
class PostCategory extends Model
{
    use HasFactory;
    use HasSlug;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'metaTags' => 'arrays',
    ];

    protected $table = 'post_category';

    protected static function newFactory() {
        return new PostCategoryFactory();
    }

    public function posts() {
        return $this->belongsToMany(Post::class, PostCategoryPivot::class);
    }

    public function parent() {
        return $this->belongsTo(PostCategory::class, 'parent_id');
    }
}
