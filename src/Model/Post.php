<?php

namespace QuangPhuc\PeaCMS\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use QuangPhuc\PeaCMS\Factory\PostFactory;
use QuangPhuc\PeaCMS\Model\Helper\HasSlug;
use QuangPhuc\PeaFilterModel\Support\HasFilter;
use QuangPhuc\PeaFilterModel\Support\HasFilterInterface;

/**
 * Class Post
 * @package QuangPhuc\PeaCMS\Model
 * @property string       $slug
 * @property string       $title
 * @property string       $introduce
 * @property string       $content
 * @property mixed[]      $metaTags
 * @property string       $type
 * @property string       $html
 * @property string       $components
 * @property string       $assets
 * @property string       $css
 * @property string       $styles
 * @property PostCategory $categories
 * @property Carbon       $created_at
 */
class Post extends Model implements HasFilterInterface {
    use HasFactory;
    use HasFilter;
    use HasSlug;

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'metaTags' => 'arrays',
    ];

    protected $table = 'post';

    protected $guarded = [];

    protected static function newFactory() {
        return new PostFactory();
    }

    public function categories() {
        return $this->belongsToMany(PostCategory::class, PostCategoryPivot::class);
    }
}
