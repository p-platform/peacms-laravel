<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post', function (Blueprint $table) {
            $table->id();
            $table->string('slug');
            $table->string('image');
            $table->string('title');
            $table->text('introduce');
            $table->text('content');

            $table->text('html')->nullable();
            $table->text('components')->nullable();
            $table->text('assets')->nullable();
            $table->text('css')->nullable();
            $table->text('styles')->nullable();

            $table->jsonb('metaTags')->default(json_encode([]));
            $table->enum('type', ['post', 'page']);
            $table->timestamps();

            $table->unique('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post');
    }
}
