<?php
/**
 * @var \QuangPhuc\PeaCMS\Model\Post $post
 */
?>
@extends('peacms::themes.default.layout.default')

@section('style')
    <style>
        {!! $post->css !!}
    </style>
@endsection

@section('content')
    {!! $post->html !!}
@endsection
