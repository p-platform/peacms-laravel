<?php
/**
 * @var \QuangPhuc\PeaCMS\Model\Post $page
 */
?>
@extends('peacms::themes.default.layout.default')

@section('style')
    <style>
        {!! $page->css !!}
    </style>
@endsection

@section('content')
    {!! $page->html !!}
@endsection
