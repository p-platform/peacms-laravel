<?php
/**
 * @var \QuangPhuc\PeaCMS\Model\Post $page
 */
?><!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Page builder</title>
    <link rel="stylesheet" href="{{ asset('css/page-builder.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<div id="gjs" data-page="{{ $page->slug }}">
    <style></style>
</div>
<script>
    window.baseUrl = '{{ asset('') }}'
</script>
<script src="{{ asset('js/page-builder.js') }}"></script>
</body>
</html>
