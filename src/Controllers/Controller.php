<?php

namespace QuangPhuc\PeaCMS\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function getView($name) {
        if ($view = view()->exists('peacms-themes.' . config('peacms.theme') . '.' . $name)) {
            return view($view);
        } elseif ($view = view()->exists('peacms:themes.' . config('peacms.theme') . '.' . $name)) {
            return view($view);
        } else {
            return view('peacms:themes.default.' . $name);
        }
    }
}
