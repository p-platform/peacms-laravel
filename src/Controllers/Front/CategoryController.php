<?php

namespace QuangPhuc\PeaCMS\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use QuangPhuc\PeaCMS\Model\PostCategory;

class CategoryController extends FrontController {
    public function index(PostCategory $category) {
        $data = [
            'category' => $category,
        ];
        return $this->render('category', $data);
    }
}
