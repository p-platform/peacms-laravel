<?php

namespace QuangPhuc\PeaCMS\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use QuangPhuc\PeaCMS\Model\Post;

class PageController extends FrontController
{
    public function index(Post $post) {
        $data = [
            'page' => $post,
        ];
        return $this->render('page', $data);
    }
}
