<?php

namespace QuangPhuc\PeaCMS\Controllers\Back;

use QuangPhuc\PeaCMS\Controllers\Controller;

class BackController extends Controller {

    protected function render($viewName, $data = [], $mergeData = []) {
        if (
            ($view = 'peacms-themes.' . config('peacms.theme') . '.' . $viewName)
            && view()->exists($view)
        ) {
            return view($view, $data, $mergeData);
        } elseif (
            ($view = 'peacms::themes.' . config('peacms.theme') . '.' . $viewName)
            && view()->exists($view)
        ) {
            return view($view, $data, $mergeData);
        } else {
            return view('peacms::themes.default.' . $viewName, $data, $mergeData);
        }
    }
}
