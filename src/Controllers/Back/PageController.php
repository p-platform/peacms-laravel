<?php

namespace QuangPhuc\PeaCMS\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use QuangPhuc\PeaCMS\Model\Post;

class PageController extends BackController
{
    public function loadPageContent(Post $post) {
        return response()->json([
            'gjs-html' => $post->html,
            'gjs-assets' => $post->assets,
            'gjs-components' => $post->components,
            'gjs-css' => $post->css,
            'gjs-styles' => $post->styles,
        ]);
    }

    public function storePageContent(Request $request, Post $post) {
        Log::error($request);
        $post->update([
            'html' => $request->post('gjs-html'),
            'assets' => $request->post('gjs-assets'),
            'components' => $request->post('gjs-components'),
            'css' => $request->post('gjs-css'),
            'styles' => $request->post('gjs-styles'),
        ]);
        dd($post);
    }

    public function uploadAssets(Request $request, Post $post) {
        return [
            'data' => [
                asset('upload/' .$request->uploadFile->store($post->slug . '/images', 'public'))
            ]
        ];
    }

    public function pageBuilder(Request $request, Post $post) {
        $data = [
            'page' => $post,
        ];
        return view('peacms::back.page-builder', $data);
    }
}
