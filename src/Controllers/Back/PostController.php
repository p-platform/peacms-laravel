<?php

namespace QuangPhuc\PeaCMS\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use QuangPhuc\PeaCMS\Model\Post;

class PostController extends BackController
{
    public function index(Post $post) {
        $data = [
            'post' => $post,
        ];
        return $this->render('post', $data);
    }
}
