<?php

namespace QuangPhuc\PeaCMS;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use QuangPhuc\PeaCMS\Model\Menu;
use QuangPhuc\PeaCMS\Model\Post;
use QuangPhuc\PeaCMS\Model\PostCategory;
use QuangPhuc\PeaCMS\Observe\MenuObserve;
use QuangPhuc\PeaCMS\Observe\PostCategoryObserver;
use QuangPhuc\PeaCMS\Observe\PostObserver;

class PeaCMSServiceProvider extends ServiceProvider {
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $this->loadMigrationsFrom(__DIR__.'/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/view', 'peacms');

        $this->publishes([
            __DIR__.'/config/peacms.php' => config_path('peacms.php'),
        ], 'config');

        $this->publishes([
            __DIR__.'/view/themes/default/' => resource_path('views/peacms-themes'),
        ], 'view');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() {
        Menu::observe(MenuObserve::class);
        Post::observe(PostObserver::class);
        PostCategory::observe(PostCategoryObserver::class);

        Route::model('peacms_post_category', PostCategory::class);
        Route::model('peacms_post', Post::class);
        Route::model('peacms_page', Post::class);
    }
}
