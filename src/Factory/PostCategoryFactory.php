<?php

namespace QuangPhuc\PeaCMS\Factory;

use QuangPhuc\PeaCMS\Model\PostCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PostCategory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'slug' => $this->faker->unique()->slug,
            'name' => $this->faker->name,
            'image' => $this->faker->imageUrl(),
            'introduce' => $this->faker->paragraph,
            'content' => $this->faker->paragraph(10),
        ];
    }
}
